package ${package}

import dev.reactant.reactant.core.component.Component

<#if hook>
import dev.reactant.reactant.core.component.lifecycle.LifeCycleHook
import dev.reactant.reactant.service.spec.dsl.register
</#if>
<#if inspector>import dev.reactant.reactant.core.component.lifecycle.LifeCycleInspector</#if>

<#if schedulerService>import dev.reactant.reactant.service.spec.server.Scheduler</#if>
<#if commandService>import dev.reactant.reactant.extra.command.PicocliCommandService</#if>
<#if uiService>import dev.reactant.reactant.ui.ReactantUIService</#if>
<#if eventService>import dev.reactant.reactant.service.spec.server.EventService</#if>

@Component
class ${className}(
    <#if commandService>
    private val commandService: PicocliCommandService,
    </#if>
    <#if eventService>
    private val eventService: EventService,
    </#if>
    <#if uiService>
    private val uiService: ReactantUIService,
    </#if>
    <#if schedulerService>
    private val scheduler: SchedulerService,
    </#if>
)<#if hook || inspector> : </#if><#if hook>LifeCycleHook</#if><#if hook && inspector>, </#if><#if inspector>LifeCycleInspector</#if> {

    <#if hook>
    override fun onEnable() {
        <#if commandService>
        register(commandService) {
            // Command Register
            // command(::TopLevelCommand) {
            //     command(::SubCommand) {
            //         command({ SubCommandWithParams(commandService) })
            //     }
            // }
        }
        </#if>
        <#if eventService>
        register(eventService) {
            // Event Register
            // PlayerJoinEvent::class.observable().subscribe { ... }
        }
        </#if>
    }
    </#if>
}
