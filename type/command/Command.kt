package ${package}

import dev.reactant.reactant.extra.command.ReactantCommand
import picocli.CommandLine

@CommandLine.Command(
        name = "${commandName}",
        aliases = [${alias}],
        mixinStandardHelpOptions = true,
        description = [${desc}]
)
internal class ${className} : ReactantCommand() {
    override fun run() {
        // requirePermission(...)
        showUsage()
    }
}
